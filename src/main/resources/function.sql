/**
 * Author:  mangprang
 * Created: Jul 19, 2022
 */

-- dynamic select function
CREATE OR REPLACE FUNCTION public.fx_select(table_param character varying)
 RETURNS table(payload json)
 AS $$
	begin
		return query execute format(
			'select row_to_json(result_query) from (select * from %I) as result_query', table_param
		);
	end;
 $$ LANGUAGE plpgsql;
 ;