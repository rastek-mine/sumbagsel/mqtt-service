package id.rastek.sumbagsel.master.util.mqtt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import id.rastek.sumbagsel.master.config.ObjectMapperConfig;
import id.rastek.sumbagsel.master.model.entity.LogReceive;
import id.rastek.sumbagsel.master.service.LogReceiveService;
import id.rastek.sumbagsel.master.service.SensorParameterService;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mangprang
 */

@Slf4j
@Component
public class MqttPushClient {

//    @Autowired
//    private MqttPushCallback pushCallback;
    @Autowired
    private MqttConfig mqttConfig;
    @Autowired
    private LogReceiveService logReceiveService;
    @Autowired
    private SensorParameterService sensorParameterService;
    @Autowired
    private ObjectMapperConfig objectMapperConfig;


    private static MqttClient client;

    public static MqttClient getClient() {
        return client;
    }

    private static void setClient(MqttClient client) {
        MqttPushClient.client = client;
    }

    /**
     * Client connection
     *
     * @param host      ip+port
     * @param clientID  Client Id
     * @param username  User name
     * @param password  Password
     * @param timeout   Timeout time
     * @param keepalive Retention number
     */
    public void connect(String host, String clientID, String username, String password, int timeout, int keepalive) {
        MqttClient client;
        try {
            log.info("[MQTT] Connecting to broker...");
            client = new MqttClient(host, clientID, new MemoryPersistence());
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(false);
            options.setAutomaticReconnect(true);
            if (!Strings.isNullOrEmpty(username)) options.setUserName(username);
            if (!Strings.isNullOrEmpty(password)) options.setPassword(password.toCharArray());
            options.setConnectionTimeout(timeout);
            options.setKeepAliveInterval(keepalive);
            MqttPushClient.setClient(client);
            try {
//                client.setCallback(new MqttPushCallback());
                client.setCallback(null);
                client.connect(options);
                log.info("[MQTT] Connected to broker");
            } catch (Exception e) {
                log.error("[MQTT]: " + e.getMessage());
            }
        } catch (Exception e) {
            log.error("[MQTT]: " + e.getMessage());
        }
    }

    /**
     * Release
     *
     * @param qos         Connection mode
     * @param retained    Whether to retain
     * @param topic       theme
     * @param pushMessage Message body
     */
    public void publish(int qos, boolean retained, String topic, String pushMessage) {
        log.info("Check connection before publishing topic");
        if (!MqttPushClient.getClient().isConnected()) {
            log.info("[MQTT] client is disconnected");
            log.info("[MQTT] client is try to reconnecting");
            mqttConfig.getMqttPushClient();
        }
        if (MqttPushClient.getClient().isConnected()) {
            log.info("[MQTT] client is connected");
        }

        MqttMessage message = new MqttMessage();
        message.setQos(qos);
        message.setRetained(retained);
        message.setPayload(pushMessage.getBytes());
        MqttTopic mTopic = MqttPushClient.getClient().getTopic(topic);
        if (null == mTopic) {
            log.error("[MQTT] topic not exist");
        }
        MqttDeliveryToken token;
        try {
            token = mTopic.publish(message);
            token.waitForCompletion();
        } catch (MqttPersistenceException e) {
            log.error("[MQTT]: " + e.getMessage());
        } catch (MqttException e) {
            log.error("[MQTT]: " + e.getMessage());
        }
    }

    /**
     *
     * @param topic theme
     */
    public void subscribe(String topic) {
        this.subscribe(topic, 1);
    }

    /**
     * Subscribe to a topic
     *
     * @param topic theme
     * @param qos   Connection mode
     */
    public void subscribe(String topic, int qos) {
        log.info("[MQTT] Start subscribing to topics " + topic);
        try {
            if (!MqttPushClient.getClient().isConnected()) {
                log.info("[MQTT] client is try to reconnecting");
                mqttConfig.getMqttPushClient();
            }

            MqttPushClient.getClient().subscribeWithResponse(topic, qos, (t, m) -> {
                // skip acknowledge from subscribe message
                if (t.equals("simpul/emon/telkomsel/ack")) return;

                String message = new String (m.getPayload());
                log.info("[MQTT] Receive message topic : " + t);
                log.info("[MQTT] Receive messages Qos : " + qos);
                log.info("[MQTT] Receive message content : " + message);

                Map<String, Object> payloadDto = objectMapperConfig.getObjectMapperConfig().readValue(message, Map.class);
                String deviceId = (String) payloadDto.getOrDefault("device_id", "");

                Object objectTime = payloadDto.getOrDefault("time", 0);
                Number time = 0;
                boolean isValid = true;
                if (!(objectTime instanceof Number)) {
                    isValid = false;
                    this.publishAck(deviceId, objectTime, "invalid time");
                }

                ZoneId utc = ZoneId.systemDefault();
                LocalDateTime formattedDate = LocalDateTime.ofInstant(Instant.ofEpochSecond(time.longValue()), utc);
                //#region insert_log
                LogReceive payload = new LogReceive();
                payload.setTopic(t);
                payload.setPayload(message);
                payload.setDeviceId(deviceId);
                payload.setDate(formattedDate);
                payload = logReceiveService.create(payload);

                if (!isValid) return;
                //#endregion

                //#region log_validation
                String ackMessage = "invalid parameter";
                isValid = sensorParameterService.validateProperties(payloadDto);
                if (isValid) {
                    sensorParameterService.create(payloadDto, payload);
                    ackMessage = "acknowledge ok";
                }
                //#endregion

                //#region acknowledge
                this.publishAck(deviceId, time, ackMessage);
                //#endregion

                log.info("--mangprang--\n\n\n");
            });

            Thread.sleep(5000);
        } catch (MqttException e) {
            log.error("[MQTT] " + e.getMessage());
        } catch (Exception e) {
            log.error("[MQTT] " + e.getMessage());
        }
    }

    private void publishAck(String deviceId, Object time, String ackMessage) throws JsonProcessingException {
        Map<String, Object> ackPayload = new HashMap<>();
        ackPayload.put("device_id", deviceId);
        ackPayload.put("time", time);
        ackPayload.put("status", ackMessage);
        String acknowledge = objectMapperConfig.getObjectMapperConfig().writeValueAsString(ackPayload);
        log.info("[MQTT] Publish Acknowledge: " + acknowledge + " is preparing message...");
        this.publish(1, false, "simpul/emon/telkomsel/ack", acknowledge);
        log.info("[MQTT] Publish Acknowledge: sent");
    }
}
