package id.rastek.sumbagsel.master.util.mqtt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author mangprang
 */

@Component
@ConfigurationProperties("mqtt")
@Getter
@Setter
public class MqttConfig {

    @Value("${mqtt.clientId}")
    private String clientID;
    @Value("${mqtt.host}")
    private String hostUrl;
    @Value("${mqtt.username}")
    private String username;
    @Value("${mqtt.password}")
    private String password;
    @Value("${mqtt.timeout}")
    private int timeout;
    @Value("${mqtt.keepalive}")
    private int keepalive;
    @Value("${mqtt.topic}")
    private String topic;

    @Autowired
    private MqttPushClient mqttPushClient;

    /**
     * Use QoS 0 when …
     * You have a completely or mostly stable connection between sender and receiver. A classic use case for QoS 0 is connecting a test client or a front end application to an MQTT broker over a wired connection.
     * You don’t mind if a few messages are lost occasionally. The loss of some messages can be acceptable if the data is not that important or when data is sent at short intervals
     * You don’t need message queuing. Messages are only queued for disconnected clients if they have QoS 1 or 2 and a persistent session.
     *
     * Use QoS 1 when …
     * You need to get every message and your use case can handle duplicates. QoS level 1 is the most frequently used service level because it guarantees the message arrives at least once but allows for multiple deliveries. Of course, your application must tolerate duplicates and be able to process them accordingly.
     * You can’t bear the overhead of QoS 2. QoS 1 delivers messages much faster than QoS 2.
     *
     * Use QoS 2 when …
     * It is critical to your application to receive all messages exactly once. This is often the case if a duplicate delivery can harm application users or subscribing clients. Be aware of the overhead and that the QoS 2 interaction takes more time to complete.
     * @return
     */
    @Bean
    public MqttPushClient getMqttPushClient() {
        mqttPushClient.connect(hostUrl, clientID, username, password, timeout, keepalive);
        mqttPushClient.subscribe(topic, 1);
        return mqttPushClient;
    }

}

