package id.rastek.sumbagsel.master.util.mqtt;

import lombok.SneakyThrows;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Deprecated
//@Component
public class MqttPushCallback implements MqttCallback {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MqttConfig mqttConfig;
//    @Autowired
//    private MqttSubMessageRepo mqttSubMessageRepo;

//    private static MqttClient client;

    @Override
    public void connectionLost(Throwable throwable) {
        log.error("[MQTT] Disconnected, " + throwable.getMessage());
        try {
            Thread.sleep(5000);
            if (MqttPushClient.getClient() == null) {
                mqttConfig.getMqttPushClient();
            } else if (!MqttPushClient.getClient().isConnected()) {
                mqttConfig.getMqttPushClient();
            }
        } catch (Exception e) {
            log.error("[MQTT] " + e.getMessage());
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        // The message you get after you subscribe will be executed here
        String message = new String(mqttMessage.getPayload());
        log.info("[MQTT] Receive message topic : " + topic);
        log.info("[MQTT] Receive messages Qos : " + mqttMessage.getQos());
        log.info("[MQTT] Receive message content : " + message);

        String[] topicParts = topic.split("/");
        switch (topicParts[topicParts.length-1]) {
            case "90": // dimming
            case "91": { // time zone
//                MqttSubMessage mqttSubMessage = new MqttSubMessage();
//                mqttSubMessage.setMessageId(mqttMessage.getId());
//                mqttSubMessage.setTopic(topic);
//                mqttSubMessage.setMessage(message);
//
//                mqttSubMessageRepo.save(mqttSubMessage);
                break;
            }
            case "92": { // status manual on/off

            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        log.info("[MQTT] Delivery Complete --------- " + iMqttDeliveryToken.isComplete());
    }

}
