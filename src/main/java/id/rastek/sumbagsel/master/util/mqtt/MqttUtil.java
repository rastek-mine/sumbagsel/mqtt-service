package id.rastek.sumbagsel.master.util.mqtt;

import java.util.concurrent.Callable;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author mangprang
 */
public class MqttUtil implements Callable<Void> {

    private static final Logger log = LoggerFactory.getLogger(MqttUtil.class);
    private IMqttClient client;
    private String topic;
    private String msg;

    public MqttUtil(IMqttClient client, String topic, String msg) {
        this.client = client;
        this.topic = topic;
        this.msg = msg;
    }

    @Override
    public Void call() throws Exception {
        if (!client.isConnected()) {
            log.info("[I31] Client not connected");
            return null;
        }

        MqttMessage msg = buildMessage();
        msg.setQos(1);
        msg.setRetained(true);
        client.publish(topic, msg);
        return null;
    }

    private MqttMessage buildMessage() {
        byte[] payload = this.msg.getBytes();
        return new MqttMessage(payload);
    }

}
