package id.rastek.sumbagsel.master.controller;

import id.rastek.sumbagsel.master.model.dto.ChartSearchDto;
import id.rastek.sumbagsel.master.model.dto.DeviceDto;
import id.rastek.sumbagsel.master.model.dto.LogReceiveDto;
import id.rastek.sumbagsel.master.service.LogReceiveService;
import id.rastek.sumbagsel.master.service.OverviewService;
import id.rastek.sumbagsel.master.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/overview")
public class OverviewController {

    @Autowired
    private OverviewService overviewService;


    @GetMapping("/energy/monitor")
    public ResponseEntity<?> getEnergyMonitor(DeviceDto filter, @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            var special = overviewService.getEnergyMonitor(filter, pageable);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/ref/parameters")
    public ResponseEntity<?> getParameters() {
        try {
            var special = overviewService.getSensorParametersRef();
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/raw")
    public ResponseEntity<?> getRaw(ChartSearchDto filter) {
        try {
            var special = overviewService.getRawData(filter);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/volume")
    public ResponseEntity<?> getVolumeTank(@RequestParam(name = "deviceId") String deviceId) {
        try {
            var special = overviewService.getVolumeTank(deviceId);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/dynamic")
    public ResponseEntity<?> getDynamicSelect(@RequestParam(name = "tableName") String tableName) {
        try {
            var special = overviewService.dynamicSelect(tableName);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

}

