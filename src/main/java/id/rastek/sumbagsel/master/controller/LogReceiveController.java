package id.rastek.sumbagsel.master.controller;

import id.rastek.sumbagsel.master.model.dto.LogReceiveDto;
import id.rastek.sumbagsel.master.service.LogReceiveService;
import id.rastek.sumbagsel.master.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/log")
public class LogReceiveController {

    @Autowired
    private LogReceiveService service;


    @GetMapping()
    public ResponseEntity<List<LogReceiveDto>> list(LogReceiveDto filter, @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) final Pageable pageable) {
        try {
            Page<LogReceiveDto> special = service.getAll(filter, pageable);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<LogReceiveDto>> list(LogReceiveDto filter) {
        try {
            List<LogReceiveDto> special = service.getAll(filter);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable String id) {
        try {
            LogReceiveDto special = service.getById(id);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable(required = true) String id, @RequestBody LogReceiveDto input) {
        try {
            LogReceiveDto result = service.getById(id);
            if (result == null) {
                return ResponseEntity.notFound().build();
            }
            LogReceiveDto special = service.update(id, input);
            return new ResponseEntity(new ApiResponse<>("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody LogReceiveDto input) {
        try {
            LogReceiveDto special = service.create(input);
            return new ResponseEntity(new ApiResponse("200", "success", special), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

}

