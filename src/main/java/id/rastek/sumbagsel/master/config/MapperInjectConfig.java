package id.rastek.sumbagsel.master.config;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = MapperDateConverter.class)
public class MapperInjectConfig {
}
