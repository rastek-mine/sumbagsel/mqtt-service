/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.config;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.mapstruct.Mapper;

/**
 *
 * @author mangprang
 */
@Mapper(componentModel = "spring")
public class MapperDateConverter {

  public Instant fromStringtoInstant(String instant) {
    if (instant == null) {
      return null;
    }
    final DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(ZoneId.of("UTC"));
    return Instant.from(formatter.parse(instant));
  }

  public String fromInstanttoString(Instant instant) {
    if (instant == null) {
      return null;
    }
    final DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(ZoneId.of("UTC"));
    return formatter.format(instant);
  }

  public UUID stringToUUID(String uuid) {
    if (uuid == null) {
      return null;
    }
    return UUID.fromString(uuid);
  }

}
