/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.exception;

import id.rastek.sumbagsel.master.util.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 * @author nt
 */
@RestControllerAdvice
public class ExceptionControllerAdviceHandler {

  @ExceptionHandler(RecordNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ApiResponse handleNoRecordFoundException(RecordNotFoundException ex) {

    ApiResponse errorResponse = new ApiResponse();
    errorResponse.setCode(String.valueOf(HttpStatus.NOT_FOUND.value()));
    errorResponse.setMessage("No Record Found");
    return errorResponse;
  }
  
  @ExceptionHandler(IdNotMatchException.class)
  @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  @ResponseBody
  public ApiResponse handleNoRecordFoundException(IdNotMatchException ex) {

    ApiResponse errorResponse = new ApiResponse();
    errorResponse.setCode(String.valueOf(HttpStatus.NOT_ACCEPTABLE.value()));
    errorResponse.setMessage("Unacceptable parameters");
    return errorResponse;
  }
}
