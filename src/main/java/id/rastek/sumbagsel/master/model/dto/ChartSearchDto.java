package id.rastek.sumbagsel.master.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ChartSearchDto implements Serializable {

  private String deviceId;
  private String parameters = "";
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate start;
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate end;
  private String sort = "asc";

}
