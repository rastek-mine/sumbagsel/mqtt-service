/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.repository;

import id.rastek.sumbagsel.master.model.entity.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface DeviceRepository extends JpaRepository<Device, String> {

    public Page<Device> findAll(Specification<Device> spec, Pageable pageable);
    public List<Device> findAll(Specification<Device> spec);

    @Query(nativeQuery = true,
            value = "SELECT cast(payload as text) from fx_select(:tableName)"
    )
    public List<String> selectDynamicTable(@Param("tableName") String tableName);
}
