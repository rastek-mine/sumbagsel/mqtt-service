/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.repository;

import id.rastek.sumbagsel.master.model.entity.SensorParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface SensorParameterRepository extends JpaRepository<SensorParameter, String> {

  public Page<SensorParameter> findAll(Specification<SensorParameter> spec, Pageable pageable);
  public List<SensorParameter> findAll(Specification<SensorParameter> spec);
  public List<SensorParameter> findAllByDeviceId(String deviceId);
  public SensorParameter findFirstByIdAndDeviceId(String id, String deviceId);
  public long countByDeviceId(String deviceId);
}
