/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.repository;

import id.rastek.sumbagsel.master.model.entity.Unit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface UnitRepository extends JpaRepository<Unit, String> {

  public Page<Unit> findAll(Specification<Unit> spec, Pageable pageable);
  public List<Unit> findAll(Specification<Unit> spec);

  @Query(nativeQuery = true,
    value = "select tu.unit_symbol from td_sensor_parameter tsp \n" +
            "join tr_unit tu on tu.unit_id = tsp.unit_id\n" +
            "where tsp.sp_id = :spId and tu.unit_sts = 1\n" +
            "limit 1"
  )
  public String getUnitSymbolBySpId(String spId);

}
