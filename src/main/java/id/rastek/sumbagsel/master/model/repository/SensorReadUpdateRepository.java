/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.repository;

import id.rastek.sumbagsel.master.model.entity.SensorReadUpdate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author mangprang
 */
public interface SensorReadUpdateRepository extends JpaRepository<SensorReadUpdate, String> {

  public Page<SensorReadUpdate> findAll(Specification<SensorReadUpdate> spec, Pageable pageable);
  public List<SensorReadUpdate> findAll(Specification<SensorReadUpdate> spec);
  public List<SensorReadUpdate> findAllByDeviceId(String deviceId);
  public Optional<SensorReadUpdate> findFirstBySpIdAndDeviceId(String spId, String deviceId);
  public long countByDeviceId(String deviceId);

}
