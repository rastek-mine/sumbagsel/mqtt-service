package id.rastek.sumbagsel.master.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ChartDto implements Serializable {

  private LocalDateTime datetime;
  private String dateUtc;
  private String dateIdn;
  private String parameter;
  private Double value;
  private String descriptiveValue;
  private String statusValue;

}
