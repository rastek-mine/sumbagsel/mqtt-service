/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.specification;

import id.rastek.sumbagsel.master.model.dto.ChartSearchDto;
import id.rastek.sumbagsel.master.model.entity.SensorReadRawData;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @author mangprang
 */

public class SensorReadRawDataSpecification extends QuerySpecification<SensorReadRawData> {

    private Specification<SensorReadRawData> byId(String param) {
        return attributeEqualObject("id", param);
    }

    private Specification<SensorReadRawData> byDeviceId(String param) {
        return attributeEqualObject("deviceId", param);
    }

    private Specification<SensorReadRawData> byParameterIds(List<String> param) {
        return attributeInList("spId", param);
    }

    private Specification<SensorReadRawData> byStartPeriod(String param) {
        return attributeGtDate("date", param);
    }

    private Specification<SensorReadRawData> byEndPeriod(String param) {
        return attributeLtDate("date", param);
    }

    public Specification<SensorReadRawData> byChartDto(ChartSearchDto filter) {
        if (filter == null) {
            return null;
        }
        return byDeviceId(filter.getDeviceId())
                ;
    }
}
