package id.rastek.sumbagsel.master.model.entity;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tm_log_receive")
@EqualsAndHashCode(callSuper = false)
public class LogReceive extends AbstractAuditableCreateEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "id", updatable = false, nullable = false)
    private String id;
    @Column(name = "topic")
    private String topic;
    @Column(name = "payload", columnDefinition = "TEXT")
    private String payload;
    @Column(name = "device_id", length = 32)
    private String deviceId;
    @Column(name = "log_date")
    private LocalDateTime date;

}
