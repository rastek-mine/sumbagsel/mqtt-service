/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.specification;

import id.rastek.sumbagsel.master.model.dto.ChartSearchDto;
import id.rastek.sumbagsel.master.model.entity.SensorRead;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.List;

/**
 * @author mangprang
 */

public class SensorReadSpecification extends QuerySpecification<SensorRead> {

    private Specification<SensorRead> bySensorReadId(String param) {
        return attributeContains("id", param);
    }

    private Specification<SensorRead> byDeviceId(String param) {
        return relationAttributeObjectEqual("log.deviceId", param);
    }

    private Specification<SensorRead> byParameterIds(List<String> param) {
        return attributeInList("spId", param);
    }

    private Specification<SensorRead> byStartPeriod(LocalDate param) {
        if (param != null) {
            return attributeGtDate("date", param.toString());
        }
        return null;
    }

    private Specification<SensorRead> byEndPeriod(LocalDate param) {
        if (param != null) {
            return attributeLtDate("date", param.toString());
        }
        return null;
    }

    public Specification<SensorRead> byChartDto(ChartSearchDto filter) {
        if (filter == null) {
            return null;
        }
        return byDeviceId(filter.getDeviceId())
                .and(byStartPeriod(filter.getStart()))
                .and(byEndPeriod(filter.getEnd()))
                ;
    }
}
