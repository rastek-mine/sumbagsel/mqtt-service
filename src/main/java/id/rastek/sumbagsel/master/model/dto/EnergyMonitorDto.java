package id.rastek.sumbagsel.master.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EnergyMonitorDto implements Serializable {

  private String deviceId;
  private String deviceName;
  private String deviceLocation;
  private Double deviceLat;
  private Double deviceLon;
  private Number totalParameter;
  private Map<String, Object> latestData;

}
