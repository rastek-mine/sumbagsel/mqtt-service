/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.specification;

import id.rastek.sumbagsel.master.model.entity.LogReceive;
import id.rastek.sumbagsel.master.model.entity.SensorRead;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author mangprang
 */

public abstract class QuerySpecification <T> {

    public Specification<T> attributeContains(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            value = "%" + value.toLowerCase() + "%";
        }
        final String finalText = value;
        return (root, query, cb) -> {
            if (finalText == null || finalText.isBlank()) {
                return null;
            }
            return cb.like(
                    cb.lower(root.get(attribute)),
                    finalText
            );
        };
    }

    public Specification<T> attributeListContains(String attribute, List<String> values) {
        if (values == null || values.isEmpty()) {
            return null;
        } else {
            Specification<T> ret = null;
            for (String val : values) {
                if (ret == null) {
                    ret = attributeContains(attribute, val);
                } else {
                    ret = ret.or(attributeContains(attribute, val));
                }
            }
            return ret;
        }
    }

    public Specification<T> attributeInList(String attribute, List<String> values) {
        if (values == null || values.isEmpty()) {
            return null;
        } else {
            Specification<T> ret = null;
            for (String val : values) {
                if (ret == null) {
                    ret = attributeEqualObject(attribute, val);
                } else {
                    ret = ret.or(attributeEqualObject(attribute, val));
                }
            }
            return ret;
        }
    }

    public Specification<T> attributeEqualBoolean(String attribute, Boolean value) {
        if (value == null) {
            return null;
        }
        final Boolean finalVal = value;
        return (root, query, cb) -> {

            return cb.equal(
                    cb.coalesce(root.get(attribute), Boolean.FALSE),
                    finalVal
            );
        };
    }

    public Specification<T> attributeEqualObject(String attribute, Object value) {
        if (value == null) {
            return null;
        }
        final Object finalVal = value;
        return (root, query, cb) -> {

            return cb.equal(root.get(attribute), finalVal);
        };
    }

    public Specification<T> attributeGtDate(String attribute, String value) {
        if (value == null || value.isBlank()) {
            return null;
        }
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDateTime ndate = LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .atStartOfDay(zoneId)
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime();
        return (root, query, cb) -> {

            return cb.greaterThanOrEqualTo(
                    root.get(attribute).as(LocalDateTime.class),
                    ndate
            );
        };
    }

    public Specification<T> attributeLtDate(String attribute, String value) {
        if (value == null || value.isBlank()) {
            return null;
        }
        ZoneId zoneId = ZoneId.of("UTC");
        LocalDateTime ndate = LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .plusDays(1)
                .atStartOfDay(zoneId)
                .withZoneSameInstant(ZoneId.of("UTC"))
                .toLocalDateTime()
                .minusNanos(1);
        return (root, query, cb) -> {

            return cb.lessThanOrEqualTo(
                    root.get(attribute).as(LocalDateTime.class),
                    ndate
            );
        };
    }

    public Specification<T> attributeEqualDate(String attribute, String value) {
        if (value == null || value.isBlank()) {
            return null;
        }
        return attributeGtDate(attribute, value).and(attributeLtDate(attribute, value));
    }

    public Specification<SensorRead> relationAttributeObjectEqual(String attribute, Object value) {
        if (value != null) {
            final Object finalValue = value;
            String[] attrParts = attribute.split("\\.");
            return (root, query, cb) -> {
                return cb.equal(
                        root.get(attrParts[0]).get(attrParts[1]),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }
}
