package id.rastek.sumbagsel.master.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Subselect("" +
        "SELECT row_number() over() as id, tm_sensor_read.log_id, td.dev_id as device_id, tm_sensor_read.sp_id, td_sensor_parameter.sp_name, tr_unit.unit_symbol \n" +
        " , tm_sensor_read.read_value, tm_sensor_read.read_date, CASE \n" +
        "\t\tWHEN tm_sensor_read.read_value >= td_sensor_parameter.sp_max_value THEN 'MAX'\n" +
        " \tWHEN tm_sensor_read.read_value <= td_sensor_parameter.sp_min_value THEN 'MIN'           \n" +
        "\t\tELSE 'NORMAL'\n" +
        "\tEND as status \t\t   \n" +
        "FROM  tm_sensor_read \n" +
        "LEFT JOIN td_sensor_parameter ON tm_sensor_read.sp_id = td_sensor_parameter.sp_id\n" +
        "LEFT JOIN tr_unit ON tr_unit.unit_id = td_sensor_parameter.unit_id\n" +
        "INNER JOIN tm_device td on td.dev_id = td_sensor_parameter.dev_id \n" +
        "where tm_sensor_read.read_date is not null\n" +
        "order by tm_sensor_read.read_date desc " +
        "")
@EqualsAndHashCode(callSuper = false)
public class SensorReadRawData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    @Column(name = "log_id")
    private String logId;
    @Column(name = "device_id")
    private String deviceId;
    @Column(name = "sp_id")
    private String spId;
    @Column(name = "sp_name")
    private String spName;
    @Column(name = "unit_symbol")
    private String unitSymbol;
    @Column(name = "read_value")
    private Double value;
    @Column(name = "read_date")
    private LocalDateTime date;
    @Column(name = "status")
    private String status;
}