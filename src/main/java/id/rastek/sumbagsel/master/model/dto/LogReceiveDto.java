package id.rastek.sumbagsel.master.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class LogReceiveDto implements Serializable {

  private String id;
  private String code;
  private Integer sortLevel;
  private String name;
  private String description;
  private String createdAt;
  private String createdBy;
  private String updatedAt;
  private String updatedBy;
  private Boolean isDeleted;

}
