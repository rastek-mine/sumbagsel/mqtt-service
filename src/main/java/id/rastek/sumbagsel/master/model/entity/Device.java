package id.rastek.sumbagsel.master.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tm_device")
@EqualsAndHashCode(callSuper = false)
public class Device extends AbstractAuditableEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "dev_id", updatable = false, nullable = false)
    private String id;
    @Column(name = "dev_name", length = 32)
    private String name;
    @Column(name = "dev_location")
    private String location;
    @Column(name = "dev_lat")
    private Double latitude = 0D;
    @Column(name = "dev_lon")
    private Double longitude = 0D;

}
