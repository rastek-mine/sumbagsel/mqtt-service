package id.rastek.sumbagsel.master.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="td_sensor_parameter")
@EqualsAndHashCode(callSuper = false)
public class SensorParameter extends AbstractAuditableEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "sp_id", updatable = false, nullable = false)
    private String id;
    @Column(name = "ts_id", length = 32)
    private String tsId;
    @Column(name = "dev_id", length = 32)
    private String deviceId;
//    @Column(name = "unit_id", length = 32)
//    private String unitId;
    @Column(name = "sp_min_value")
    private Double minValue;
    @Column(name = "sp_max_value")
    private Double maxValue;
    @Column(name = "sp_min_warm")
    private Double minWarm;
    @Column(name = "sp_max_warm")
    private Double maxWarm;
    @Column(name = "sp_name", length = 128)
    private String name;
    @Column(name = "sp_address", length = 32)
    private String address;
    @Column(name = "sp_sts", length = 1)
    private Integer status;
    @Column(name = "sp_update")
    private LocalDateTime update;

}
