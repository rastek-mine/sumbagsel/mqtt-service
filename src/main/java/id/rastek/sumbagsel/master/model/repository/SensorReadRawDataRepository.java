/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.repository;

import id.rastek.sumbagsel.master.model.entity.SensorReadRawData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author mangprang
 */
public interface SensorReadRawDataRepository extends JpaRepository<SensorReadRawData, String> {

  public Page<SensorReadRawData> findAll(Specification<SensorReadRawData> spec, Pageable pageable);
  public List<SensorReadRawData> findAll(Specification<SensorReadRawData> spec);
  List<SensorReadRawData> findAllByDeviceIdAndLogId(String deviceId, String logId);

}
