package id.rastek.sumbagsel.master.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@Entity
@Table(name="tr_sensor_type")
@EqualsAndHashCode(callSuper = false)
public class SensorType extends AbstractAuditableEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ts_id", updatable = false, nullable = false)
    private String id;
//    @Column(name = "tenant_id", length = 32)
//    private String tenantId;
    @Column(name = "ts_name", length = 64)
    private String name;
    @Column(name = "ts_min_value")
    private Double minValue;
    @Column(name = "ts_max_value")
    private Double maxValue;
    @Column(name = "ts_min_warm")
    private Double minWarm;
    @Column(name = "ts_max_warm")
    private Double maxWarm;
    @Column(name = "ts_scale", length = 32)
    private String scale;
    @Column(name = "ts_sts", length = 1)
    private Integer status;
    @Column(name = "ts_update")
    private LocalDateTime update;

    public SensorType() {
        setId(UUID.randomUUID().toString());
    }
}
