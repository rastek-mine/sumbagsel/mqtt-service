/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.specification;

import id.rastek.sumbagsel.master.model.dto.DeviceDto;
import id.rastek.sumbagsel.master.model.entity.Device;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author mangprang
 */

public class DeviceSpecification extends QuerySpecification<Device> {

    private Specification<Device> byDeviceId(String param) {
        return attributeContains("id", param);
    }

    private Specification<Device> byPayload(String param) {
        return attributeContains("payload", param);
    }

    public Specification<Device> byEntitySearch(DeviceDto filter) {
        if (filter == null) {
            return null;
        }
        return byDeviceId(filter.getDeviceId())
                ;
    }
}
