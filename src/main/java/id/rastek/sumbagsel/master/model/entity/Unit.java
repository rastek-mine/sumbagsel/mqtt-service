package id.rastek.sumbagsel.master.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tr_unit")
@EqualsAndHashCode(callSuper = false)
public class Unit extends AbstractAuditableEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "unit_id", updatable = false, nullable = false)
    private String id;
    @Column(name = "tenant_id", length = 32)
    private String tenantId;
    @Column(name = "unit_name")
    private String name;
    @Column(name = "unit_symbol")
    private String symbol;
    @Column(name = "unit_sts")
    private Integer sts;

}
