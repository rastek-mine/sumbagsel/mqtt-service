package id.rastek.sumbagsel.master.model.mapper;

import id.rastek.sumbagsel.master.config.MapperInjectConfig;
import id.rastek.sumbagsel.master.model.dto.LogReceiveDto;
import id.rastek.sumbagsel.master.model.entity.LogReceive;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface LogReceiveMapper {

    @Mappings({
            
    })
    public LogReceiveDto toDto(LogReceive entity);

    public LogReceive toEntity(LogReceiveDto dto);

}
