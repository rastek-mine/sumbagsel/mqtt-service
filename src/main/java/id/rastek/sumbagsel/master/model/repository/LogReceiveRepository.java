/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.repository;

import id.rastek.sumbagsel.master.model.entity.LogReceive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author mangprang
 */
public interface LogReceiveRepository extends JpaRepository<LogReceive, String> {

  public Page<LogReceive> findAll(Specification<LogReceive> spec, Pageable pageable);
  public List<LogReceive> findAll(Specification<LogReceive> spec);

  @Query(nativeQuery = true,
    value = "select t.id from tm_log_receive t \n" +
            "inner join tm_sensor_read tsr on tsr.log_id = t.id \n" +
            "where device_id = :deviceId and t.log_date >= :startDate and t.log_date <= :endDate \n" +
            "order by t.log_date desc limit 1"
  )
  public String findIdOrderDescBy(String deviceId, LocalDateTime startDate, LocalDateTime endDate);

}
