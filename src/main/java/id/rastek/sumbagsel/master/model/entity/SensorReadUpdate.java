package id.rastek.sumbagsel.master.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tm_sensor_read_update")
@EqualsAndHashCode(callSuper = false)
public class SensorReadUpdate extends AbstractAuditableCreateEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "read_update_id", updatable = false, nullable = false)
    private String id;
    @Column(name = "sp_id", length = 32)
    private String spId;
    @Column(name = "dev_id", length = 32)
    private String deviceId;
    @Column(name = "read_update_date")
    private LocalDateTime date;
    @Column(name = "read_update_value")
    private Double value;

}
