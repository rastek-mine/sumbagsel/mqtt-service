package id.rastek.sumbagsel.master.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tm_sensor_read")
@EqualsAndHashCode(callSuper = false)
public class SensorRead extends AbstractAuditableCreateEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "read_id", updatable = false, nullable = false)
    private String id;
    @Column(name = "sp_id", length = 32)
    private String spId;
    @Column(name = "read_date", length = 64)
    private LocalDateTime date;
    @Column(name = "read_value")
    private Double value;
    @Column(name = "read_sts", length = 1)
    private Integer status;

    @ManyToOne
    @JoinColumn(name = "log_id")
    private LogReceive log;

}
