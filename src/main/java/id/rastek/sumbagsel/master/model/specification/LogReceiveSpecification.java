/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.model.specification;

import id.rastek.sumbagsel.master.model.dto.LogReceiveDto;
import id.rastek.sumbagsel.master.model.entity.LogReceive;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author mangprang
 */

public class LogReceiveSpecification extends QuerySpecification<LogReceive> {

    private Specification<LogReceive> byTopic(String param) {
        return attributeContains("topic", param);
    }

    private Specification<LogReceive> byPayload(String param) {
        return attributeContains("payload", param);
    }

    public Specification<LogReceive> byEntitySearch(LogReceiveDto filter) {
        if (filter == null) {
            return null;
        }
        return byTopic(filter.getName())
                .and(byPayload(filter.getCode()))
                ;
    }
}
