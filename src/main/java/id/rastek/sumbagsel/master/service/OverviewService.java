/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import id.rastek.sumbagsel.master.model.dto.ChartDto;
import id.rastek.sumbagsel.master.model.dto.ChartSearchDto;
import id.rastek.sumbagsel.master.model.dto.DeviceDto;
import id.rastek.sumbagsel.master.model.dto.EnergyMonitorDto;
import id.rastek.sumbagsel.master.model.entity.*;
import id.rastek.sumbagsel.master.model.repository.*;
import id.rastek.sumbagsel.master.model.specification.DeviceSpecification;
import id.rastek.sumbagsel.master.model.specification.SensorReadRawDataSpecification;
import id.rastek.sumbagsel.master.model.specification.SensorReadSpecification;
import id.rastek.sumbagsel.master.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author mangprang
 */

@Slf4j
@Service
public class OverviewService {

	@Autowired
	private DeviceRepository deviceRepository;
	@Autowired
	private UnitRepository unitRepository;
	@Autowired
	private SensorParameterRepository sensorParameterRepository;
	@Autowired
	private SensorReadUpdateRepository sensorReadUpdateRepository;
	@Autowired
	private LogReceiveRepository logReceiveRepository;
	@Autowired
	private SensorReadRawDataRepository rawDataRepository;

	public DeviceSpecification getSpecification() {
		return new DeviceSpecification();
	}
	public SensorReadSpecification sensorReadSpecification() {
		return new SensorReadSpecification();
	}
	public SensorReadRawDataSpecification sensorReadRawDataSpecification() {
		return new SensorReadRawDataSpecification();
	}

	public Page<EnergyMonitorDto> getEnergyMonitor(DeviceDto deviceDto, Pageable pageable) {
		Specification<Device> specification = getSpecification().byEntitySearch(deviceDto);
		Page<Device> devices = deviceRepository.findAll(specification, pageable);

		return devices.map(dev -> {
			Map<String, Object> latestData = new HashMap<>();
			List<SensorReadUpdate> currents = sensorReadUpdateRepository.findAllByDeviceId(dev.getId());
			List<SensorParameter> parameters = sensorParameterRepository.findAllByDeviceId(dev.getId());

			parameters.forEach(param -> {
				SensorReadUpdate data = currents.stream().filter(q -> q.getSpId().equals(param.getId()))
						.findFirst().orElse(new SensorReadUpdate());
				String symbol = unitRepository.getUnitSymbolBySpId(param.getId());
				String unit = symbol == null ? "" : " " + symbol;
				String value = data.getValue() == null ? null : data.getValue() + unit;
				latestData.put(param.getId(), value);
			});

			if (!currents.isEmpty()) {
				latestData.put("date_utc", DateUtil.localDateTimeToStringUTC(currents.get(0).getDate()));
				latestData.put("date_idn", DateUtil.localDateTimeToStringIDN(currents.get(0).getDate()));
			}

			EnergyMonitorDto dto = new EnergyMonitorDto();
			dto.setDeviceId(dev.getId());
			dto.setDeviceName(dev.getName());
			dto.setDeviceLocation(dev.getLocation());
			dto.setDeviceLat(dev.getLatitude());
			dto.setDeviceLon(dev.getLongitude());
			dto.setTotalParameter(parameters.size());
			dto.setLatestData(latestData);

			return dto;
		});
	}

	public List<String> getSensorParametersRef() {
		return sensorParameterRepository.findAll().stream().map(SensorParameter::getId).collect(Collectors.toList());
	}

	public List<ChartDto> getRawData(ChartSearchDto filter) {
		List<ChartDto> result = new ArrayList<>();

		if (filter.getStart() == null) filter.setStart(LocalDate.now());
		if (filter.getEnd() == null) filter.setEnd(LocalDate.now());

		int startY = filter.getStart().getYear();
		int startM = filter.getStart().getMonthValue();
		int startD = filter.getStart().getDayOfMonth();
		int endY = filter.getEnd().getYear();
		int endM = filter.getEnd().getMonthValue();
		int endD = filter.getEnd().getDayOfMonth();

		List<String> logIds = new ArrayList<>();
		ZoneId zoneId = ZoneId.of("UTC");

		for (int year = startY; year <= endY; year++) {
			for (int month = startM; month <= endM ; month++) {
				for (int day = startD; day <= endD; day++) {
					LocalDate date = LocalDate.of(year, month, day);
					LocalDateTime startPeriod = date
							.atStartOfDay(zoneId)
							.withZoneSameInstant(ZoneId.of("UTC"))
							.toLocalDateTime();
					LocalDateTime endPeriod = date
							.plusDays(1)
							.atStartOfDay(zoneId)
							.withZoneSameInstant(ZoneId.of("UTC"))
							.toLocalDateTime()
							.minusNanos(1);
					String logId = logReceiveRepository.findIdOrderDescBy(filter.getDeviceId(), startPeriod, endPeriod);
					logIds.add(logId);
				}
			}
		}
		log.info("[RAW DATA]: log ids " + logIds);

		List<String> parameters = Strings.isNullOrEmpty(filter.getParameters())
				? new ArrayList<>()
				: Arrays.asList(filter.getParameters().split(","));
		log.info("[RAW DATA]: selected parameters " + parameters);

		logIds.forEach(log -> {
			List<SensorReadRawData> list = rawDataRepository.findAllByDeviceIdAndLogId(filter.getDeviceId(), log);
			Stream<SensorReadRawData> streamData = list.stream();
			if (!parameters.isEmpty())
				streamData = list.stream().filter(q -> parameters.contains(q.getSpId()));

			streamData.forEach(q -> {
				ChartDto dto = new ChartDto();
				dto.setDatetime(q.getDate());
				dto.setDateUtc(DateUtil.localDateTimeToStringUTC(q.getDate()));
				dto.setDateIdn(DateUtil.localDateTimeToStringIDN(q.getDate()));
				dto.setParameter(q.getSpName());
				dto.setValue(q.getValue());
				dto.setDescriptiveValue(q.getValue() + " " + q.getUnitSymbol());
				dto.setStatusValue(q.getStatus());

				result.add(dto);
			});
		});

		Comparator<ChartDto> resultComparator = Comparator.comparing(ChartDto::getDatetime);
		if (filter.getSort().equals("desc"))
			resultComparator = Comparator.comparing(ChartDto::getDatetime, Comparator.reverseOrder());
		result.sort(resultComparator);
		return result;
	}

	public Map<String, Object> getVolumeTank(String deviceId) {
		SensorReadUpdate found = sensorReadUpdateRepository.findFirstBySpIdAndDeviceId("fuel", deviceId).orElse(new SensorReadUpdate());
		double height = 0;
		if (found.getValue() != null) height = found.getValue() / 10;	// convert mm to cm
		double length = 150;
		double width = 100;
		double volume = length * width * 0.7854 * height;
		double literVolume = volume / 1000;
		BigDecimal bd = new BigDecimal(literVolume).setScale(2, RoundingMode.HALF_UP);

		Map<String, Object> map = new HashMap<>();
		map.put("device_id", deviceId);
		map.put("height", height + " cm");
		map.put("length", length + " cm");
		map.put("width", width + " cm");
		map.put("volume", bd + " L");
		return map;
	}

	public List<Map<String, Object>> dynamicSelect(String tableName) {
		try {
			List<Map<String, Object>> payload = new ArrayList<>();
			List<String> list = deviceRepository.selectDynamicTable(tableName);
			list.forEach(data -> {
				try {
					Map<String, Object> map = new ObjectMapper().readValue(data, HashMap.class);
					payload.add(map);
				} catch (JsonProcessingException e) {
					log.error("[Dynamic Select]: Json Processing error " + e.getMessage());
					throw new ResponseStatusException(500, e.getMessage(), e);
				}
			});

			return payload;
		} catch (Exception e) {
			log.error("[Dynamic Select]: Cannot retrieve query, error " + e.getMessage());
			throw new ResponseStatusException(500, e.getMessage(), e);
		}
	}

	private String getParameterStatus(SensorParameter parameter, Double value) {
		double maxValue = parameter.getMaxValue();
		double minValue = parameter.getMinValue();

		if (value >= maxValue) {
			return "MAX";
		} else if (value <= minValue) {
			return "MIN";
		} else {
			return "NORMAL";
		}
	}

}

