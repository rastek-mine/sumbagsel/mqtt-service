/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.service;

import id.rastek.sumbagsel.master.model.entity.LogReceive;
import id.rastek.sumbagsel.master.model.entity.SensorParameter;
import id.rastek.sumbagsel.master.model.entity.SensorRead;
import id.rastek.sumbagsel.master.model.entity.SensorReadUpdate;
import id.rastek.sumbagsel.master.model.repository.SensorParameterRepository;
import id.rastek.sumbagsel.master.model.repository.SensorReadRepository;
import id.rastek.sumbagsel.master.model.repository.SensorReadUpdateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

/**
 *
 * @author mangprang
 */

@Slf4j
@Service
public class SensorParameterService {

	@Autowired
	private SensorParameterRepository repository;
	@Autowired
	private SensorReadRepository sensorReadRepository;
	@Autowired
	private SensorReadUpdateRepository sensorReadUpdateRepository;

	public boolean validateProperties(Map<String, Object> payloadDto) {
		if (payloadDto == null && payloadDto.isEmpty()) return false;

		String deviceId = (String) payloadDto.getOrDefault("device_id", "");
		if (deviceId.isBlank()) return false;

		boolean isValid = true;
		List<String> sensorProperties = repository.findAllByDeviceId(deviceId)
				.stream()
				.map(SensorParameter::getId).collect(Collectors.toList());
		List<String> dtoProperties = new ArrayList<>(payloadDto.keySet());
		log.info("Sensor Parameter: validating properties...");
		for (String prop : sensorProperties) {
			isValid = dtoProperties.contains(prop);
			if (!isValid) break;
		}

		log.info(String.format("Sensor Parameter: %s", sensorProperties.toString()));
		if (isValid) log.info(String.format("Payload Properties: %s -- are valid", dtoProperties.toString()));
		else log.error(String.format("Payload Properties: %s -- are invalid", dtoProperties.toString()));

		return isValid;
	}

	@Transactional
	public void create(Map<String, Object> payloadDto, LogReceive logReceive) {
		String deviceId = (String) payloadDto.getOrDefault("device_id", "");
		Number time = (Number) payloadDto.getOrDefault("time", 0);

		ZoneId idn = TimeZone.getTimeZone("GMT+7:00").toZoneId();
		ZoneId utc = ZoneId.systemDefault();

		List<String> sensorProperties = repository.findAllByDeviceId(deviceId)
				.stream()
				.map(SensorParameter::getId).collect(Collectors.toList());
		try {
			sensorProperties.forEach(spId -> {
				Object raw = payloadDto.getOrDefault(spId, "0");
				LocalDateTime formattedDate = LocalDateTime.ofInstant(Instant.ofEpochSecond(time.longValue()), utc);

				//#region sensor_read_update
				this.createOrUpdateSensorReadUpdate(deviceId, spId, this.getDoubleValue(raw), formattedDate);
				//#endregion

				SensorRead read = new SensorRead();
				read.setLog(logReceive);
				read.setSpId(spId);
				read.setStatus(1);
				read.setValue(this.getDoubleValue(raw));
				read.setDate(formattedDate);
				sensorReadRepository.save(read);
			});
			log.info("Sensor Read: successfully saved");
		} catch (Exception e) {
			log.error(String.format("Sensor Read: save data failed \n--%s", e.getMessage()));
		}
	}

	@Transactional
	private void createOrUpdateSensorReadUpdate(String deviceId, String spId, double value, LocalDateTime dateTime) {
		try {
			SensorReadUpdate found = sensorReadUpdateRepository.findFirstBySpIdAndDeviceId(spId, deviceId)
					.orElse(new SensorReadUpdate());
			found.setDeviceId(deviceId);
			found.setSpId(spId);
			found.setValue(value);
			found.setDate(dateTime);
			sensorReadUpdateRepository.save(found);
			log.info(String.format("Sensor Read Update: successfully saved, %s(%s = %,.2f)", deviceId, spId, value));
		} catch (Exception e) {
			log.error(String.format("Sensor Read Update: save data failed \n--%s", e.getMessage()));
		}
	}

	private Double getDoubleValue(Object value) {
		Double ret = 0D;
		if( value != null ) {
			if( value instanceof String ) {
				ret = Double.parseDouble( (String) value );
			} else if( value instanceof Number ) {
				ret = ((Number)value).doubleValue();
			} else {
				throw new ClassCastException("Not possible to cast ["+value+"] from class "+value.getClass()+" into a Double.");
			}
		}
		return ret;
	}

}

