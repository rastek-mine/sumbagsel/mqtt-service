/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.rastek.sumbagsel.master.service;

import id.rastek.sumbagsel.master.model.dto.LogReceiveDto;
import id.rastek.sumbagsel.master.model.entity.LogReceive;
import id.rastek.sumbagsel.master.model.mapper.LogReceiveMapper;
import id.rastek.sumbagsel.master.model.repository.LogReceiveRepository;
import id.rastek.sumbagsel.master.model.specification.LogReceiveSpecification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 *
 * @author mangprang
 */

@Slf4j
@Service
public class LogReceiveService {

	@Autowired
	private LogReceiveRepository repo;
	@Autowired
	private LogReceiveMapper mapper;

	public LogReceiveSpecification getSpecification() {
		return new LogReceiveSpecification();
	}

	public List<LogReceiveDto> getAll() {
		return StreamSupport.stream(repo.findAll().spliterator(), false)
				.map(mapper::toDto)
				.collect(Collectors.toList());
	}

	public List<LogReceiveDto> getAll(LogReceiveDto filter) {
		Specification<LogReceive> specification = getSpecification().byEntitySearch(filter);
		return StreamSupport.stream(repo.findAll(specification).spliterator(), false)
				.map(mapper::toDto)
				.collect(Collectors.toList());
	}

	public Page<LogReceiveDto> getAll(LogReceiveDto filter, Pageable pageable) {
		Specification<LogReceive> specification = getSpecification().byEntitySearch(filter);
		return repo.findAll(specification, pageable)
				.map(mapper::toDto);
	}

	public LogReceiveDto getById(String id) {
		LogReceive entity = repo.findById(id).orElseThrow();
		return mapper.toDto(entity);
	}

	@Transactional(readOnly = false)
	public LogReceiveDto update(String id, LogReceiveDto input) {
		LogReceive old = repo.findById(id).orElseThrow();
		LogReceive updater = mapper.toEntity(input);
		LogReceive updated = this.mapper(old, updater);
		updated = repo.save(updated);
		return mapper.toDto(updated);
	}

	@Transactional(readOnly = false)
	public LogReceiveDto create(LogReceiveDto input) {
		LogReceive entity = mapper.toEntity(input);
		entity = repo.save(entity);
		return mapper.toDto(entity);
	}

	@Transactional(readOnly = false)
	public LogReceive create(LogReceive entity) {
		try {
			entity = repo.save(entity);
			return entity;
		} catch (Exception e) {
			log.error("["+this.getClass() + "] trying to save data -- " + e.getMessage());
			throw new RuntimeException();
		}
	}

	private LogReceive mapper(LogReceive old, LogReceive updated) {
		LogReceive ret = old;
		if (updated.getTopic() != null && !updated.getTopic().isBlank()) {
			ret.setTopic(updated.getTopic());
		}
		if (updated.getPayload() != null && !updated.getPayload().isBlank()) {
			ret.setPayload(updated.getPayload());
		}

		return ret;
	}

}

